from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *

class Plugin:
    def __init__(self, iface):
        self.iface = iface

    def initGui(self):
        # create action that will start plugin configuration
        self.action = QAction("Sectioner", self.iface.mainWindow())
        self.action.setObjectName("sectioner")
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)

        # add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&Sectioner", self.action)

    def unload(self):
        # remove the plugin menu item and icon
        self.iface.removePluginMenu("&Sectioner", self.action)
        self.iface.removeToolBarIcon(self.action)

    def run(self):
        question_result = QInputDialog.getInt(self.iface.mainWindow(), 'sectioner tiles', 'select tiles number, 10 will be 10x10 tiles', value=10)
        question2_result = QInputDialog.getDouble(self.iface.mainWindow(), 'sectioner tiles', 'select overlap ratio - 1,2 will be 20% tiles overlap. Keep 1,0 for no-overlap', value=1)
        n = question_result[0]
        overlap = question2_result[0]
        iface = self.iface
        lyr = iface.activeLayer()
        extent = lyr.extent()
        crs = lyr.crs()
        width = lyr.width()
        height = lyr.height()
        x = extent.xMinimum()
        y = extent.yMinimum()
        xStep = extent.width() / n
        yStep = extent.height() / n
        renderer = lyr.renderer()
        provider = lyr.dataProvider()

        target_dir = QFileDialog.getExistingDirectory(self.iface.mainWindow(), caption='Select Output Directory')
        template_output = target_dir + '/{}-{}.tif'

        for ix in range(n):
            for iy in range(n):
                xStart = x + (ix * xStep)
                yStart = y + (iy * yStep)
                rect = QgsRectangle (xStart, yStart, xStart + xStep * overlap, yStart + yStep * overlap)
                writer = QgsRasterFileWriter(template_output.format(ix, iy))
                pipe = QgsRasterPipe()
                pipe.set(provider.clone())
                pipe.set(renderer.clone())
                writer.writeRaster(pipe, int(width / n), int(height / n), rect, lyr.crs())
