; the next section is mandatory

[general]
name=Sectioner
email=pierremarc07@gmail.com
author=Pierre Marchand
qgisMinimumVersion=2.0
description=tiling
about=tiling
version=version 0.1
tracker=https://phab.atelier-carographique.be
repository=https://phab.atelier-carographique.be
; end of mandatory metadata

; start of optional metadata
category=Raster

; Tags are in comma separated value format, spaces are allowed within the
; tag name.
; Tags should be in English language. Please also check for existing tags and
; synonyms before creating a new one.
tags=tiles,raster,waend

; these metadata can be empty, they will eventually become mandatory.
homepage=https://phab.atelier-carographique.be

; experimental flag (applies to the single version)
experimental=True

; deprecated flag (applies to the whole plugin and not only to the uploaded version)
deprecated=False

; if empty, it will be automatically set to major version + .99
qgisMaximumVersion=2.99